package ru.t1.schetinin.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class ProjectClearRequest extends AbstractUserRequest {

    public ProjectClearRequest(@Nullable final String token) {
        super(token);
    }

}
