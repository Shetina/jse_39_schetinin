package ru.t1.schetinin.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public class UserLockResponse extends AbstractUserResponse {

    public UserLockResponse(@Nullable final User user) {
        super(user);
    }

}