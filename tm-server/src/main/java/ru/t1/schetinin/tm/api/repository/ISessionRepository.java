package ru.t1.schetinin.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.model.Session;

import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO session_table (id, date, user_id, role)" +
            " VALUES (#{id}, #{date}, #{userId}, #{role})")
    void add(@NotNull Session session);

    @Insert("INSERT INTO session_table (id, date, user_id, role)" +
            " VALUES (#{session.id}, #{session.date}, #{userId}, #{session.role})")
    void addWithUserId(@NotNull @Param("userId") String userId, @NotNull @Param("session") Session session);

    @Delete("DELETE FROM session_table WHERE user_id = #{userId}")
    void clear(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM session_table WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<Session> findAll(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM session_table WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable Session findOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Select("SELECT * FROM session_table WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable Session findOneByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Select("SELECT COUNT(*) FROM session_table WHERE user_id = #{userId}")
    int getSize(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM session_table WHERE user_id = #{userId} AND id = #{id}")
    void remove(@NotNull Session session);

    @Update("UPDATE session_table SET name = #{name}, date = #{date}, role = #{role} WHERE id = #{id}")
    void update(@NotNull Session session);

}