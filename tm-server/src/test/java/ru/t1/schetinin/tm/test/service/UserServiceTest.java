package ru.t1.schetinin.tm.test.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.schetinin.tm.api.service.*;
import ru.t1.schetinin.tm.enumerated.Role;
import ru.t1.schetinin.tm.exception.field.*;
import ru.t1.schetinin.tm.exception.user.UserNotFoundException;
import ru.t1.schetinin.tm.marker.UnitCategory;
import ru.t1.schetinin.tm.model.User;
import ru.t1.schetinin.tm.service.*;
import ru.t1.schetinin.tm.util.HashUtil;

import java.util.List;

@Category(UnitCategory.class)
public class UserServiceTest {

    @NotNull
    public final static String USER_TEST_LOGIN = "USER_TEST_LOGIN";

    @NotNull
    public final static String USER_TEST_PASSWORD = "USER_TEST_PASSWORD";

    @NotNull
    public final static String USER_TEST_EMAIL = "USER_TEST@MAIL";

    @NotNull
    public final static String ADMIN_TEST_LOGIN = "ADMIN_TEST_LOGIN";

    @NotNull
    public final static String ADMIN_TEST_PASSWORD = "ADMIN_TEST_PASSWORD";

    @NotNull
    public final static String ADMIN_TEST_EMAIL = "ADMIN_TEST@MAIL";

    @NotNull
    public final static User USER_TEST = new User();

    @NotNull
    public final static User ADMIN_TEST = new User();

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static final IProjectService PROJECT_SERVICE = new ProjectService(CONNECTION_SERVICE);

    @NotNull
    private static final ITaskService TASK_SERVICE = new TaskService(CONNECTION_SERVICE);

    @NotNull
    private static final IUserService USER_SERVICE = new UserService(PROPERTY_SERVICE, CONNECTION_SERVICE, PROJECT_SERVICE, TASK_SERVICE);

    @BeforeClass
    public static void setUp() {
        USER_TEST.setLogin(USER_TEST_LOGIN);
        USER_TEST.setPasswordHash(HashUtil.salt(PROPERTY_SERVICE, USER_TEST_PASSWORD));
        USER_TEST.setEmail(USER_TEST_EMAIL);
        ADMIN_TEST.setLogin(ADMIN_TEST_LOGIN);
        ADMIN_TEST.setPasswordHash(HashUtil.salt(PROPERTY_SERVICE, ADMIN_TEST_PASSWORD));
        ADMIN_TEST.setEmail(ADMIN_TEST_EMAIL);
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable User user = USER_SERVICE.findOneById(USER_TEST.getId());
        if (user != null) USER_SERVICE.remove(user);
        @Nullable User user2 = USER_SERVICE.findOneById(ADMIN_TEST.getId());
        if (user2 != null) USER_SERVICE.remove(user2);
    }

    @Before
    public void initDemoData() throws Exception {
        USER_SERVICE.add(USER_TEST);
        USER_SERVICE.add(ADMIN_TEST);
    }

    @After
    public void clearData() throws Exception {
        USER_SERVICE.remove(USER_TEST);
        USER_SERVICE.remove(ADMIN_TEST);
    }

    @Test
    public void testFindByLogin() throws Exception {
        @Nullable final User user = USER_SERVICE.findByLogin(USER_TEST.getLogin());
        Assert.assertNotNull(user);
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.findByLogin(null));
    }

    @Test
    public void testFindByEmail() throws Exception {
        @Nullable final User user = USER_SERVICE.findByEmail(USER_TEST.getEmail());
        Assert.assertNotNull(user);
        Assert.assertThrows(EmailEmptyException.class, () -> USER_SERVICE.findByEmail(null));
    }

    @Test
    public void testIsLoginExist() throws Exception {
        Assert.assertTrue(USER_SERVICE.isLoginExists(USER_TEST.getLogin()));
    }

    @Test
    public void testIsEmailExist() throws Exception {
        Assert.assertTrue(USER_SERVICE.isEmailExists(USER_TEST.getEmail()));
    }

    @Test
    public void testAddUser() throws Exception {
        @NotNull final User user = new User();
        USER_SERVICE.add(user);
        Assert.assertNotNull(USER_SERVICE.findOneById(user.getId()));
    }

//    @Test
//    public void testClear() throws Exception {
//        USER_SERVICE.clear();
//        Assert.assertEquals(0, USER_SERVICE.getSize());
//    }

    @Test
    public void testFindAll() throws Exception {
        @NotNull final List<User> users = USER_SERVICE.findAll();
        Assert.assertEquals(users.size(), USER_SERVICE.getSize());
    }

    @Test
    public void testFindOneById() throws Exception {
        @NotNull final List<User> users = USER_SERVICE.findAll();
        @NotNull final User user1 = users.get(0);
        @NotNull final String projectId = user1.getId();
        Assert.assertEquals(user1.getId(), USER_SERVICE.findOneById(projectId).getId());
    }

    @Test
    public void testRemoveById() throws Exception {
        USER_SERVICE.removeById(ADMIN_TEST.getId());
        Assert.assertNull(USER_SERVICE.findOneById(ADMIN_TEST.getId()));
    }

    @Test
    public void testCreateUser() throws Exception {
        @NotNull final User user = USER_SERVICE.create("log", "pas");
        Assert.assertEquals("log", user.getLogin());
        Assert.assertEquals(HashUtil.salt(PROPERTY_SERVICE, "pas"), user.getPasswordHash());
        USER_SERVICE.remove(user);
    }

    @Test
    public void testCreateUserEmail() throws Exception {
        @NotNull final User user = USER_SERVICE.create("log", "pas", "email");
        Assert.assertEquals("log", user.getLogin());
        Assert.assertEquals(HashUtil.salt(PROPERTY_SERVICE, "pas"), user.getPasswordHash());
        Assert.assertEquals("email", user.getEmail());
        USER_SERVICE.remove(user);
    }

    @Test
    public void testCreateUserRole() throws Exception {
        @NotNull final User user = USER_SERVICE.create("log", "pas", Role.ADMIN);
        Assert.assertEquals("log", user.getLogin());
        Assert.assertEquals(HashUtil.salt(PROPERTY_SERVICE, "pas"), user.getPasswordHash());
        Assert.assertEquals(Role.ADMIN, user.getRole());
        USER_SERVICE.remove(user);
    }

    @Test
    public void testCreateUserNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.create(null, "STRING_FAKE", "STRING_FAKE"));
        Assert.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.create("STRING_FAKE", null, "STRING_FAKE"));
        @NotNull final User user = ADMIN_TEST;
        Assert.assertThrows(ExistsLoginException.class, () -> USER_SERVICE.create(user.getLogin(), "STRING_FAKE", "STRING_FAKE"));
        Assert.assertThrows(ExistsEmailException.class, () -> USER_SERVICE.create("STRING_FAKE", "ID_FAKE", user.getEmail()));
    }

    @Test
    public void testRemoveByLogin() throws Exception {
        USER_SERVICE.removeByLogin(ADMIN_TEST_LOGIN);
        Assert.assertNull(USER_SERVICE.findOneById(ADMIN_TEST.getId()));
    }

    @Test
    public void testRemoveByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.removeByLogin(null));
    }

    @Test
    public void testRemoveOne() throws Exception {
        @NotNull final User user = ADMIN_TEST;
        USER_SERVICE.remove(user);
        Assert.assertNull(USER_SERVICE.findOneById(user.getId()));
    }

    @Test
    public void testSetPassword() throws Exception {
        USER_SERVICE.setPassword(USER_TEST.getId(), ADMIN_TEST_PASSWORD);
        @NotNull final User user = USER_SERVICE.findOneById(USER_TEST.getId());
        Assert.assertEquals(ADMIN_TEST.getPasswordHash(), user.getPasswordHash());
        USER_SERVICE.setPassword(USER_TEST.getId(), USER_TEST_PASSWORD);
    }

    @Test
    public void testSetPasswordNegative() {
        Assert.assertThrows(IdEmptyException.class, () -> USER_SERVICE.setPassword(null, "STRING_FAKE"));
        Assert.assertThrows(PasswordEmptyException.class, () -> USER_SERVICE.setPassword("STRING_FAKE", null));
        Assert.assertThrows(UserNotFoundException.class, () -> USER_SERVICE.setPassword("STRING_FAKE", "STRING_FAKE"));
    }

    @Test
    public void testLockUserByLogin() throws Exception {
        USER_SERVICE.lockUserByLogin(USER_TEST_LOGIN);
        @NotNull final User user = USER_SERVICE.findOneById(USER_TEST.getId());
        Assert.assertTrue(user.getLocked());
    }

    @Test
    public void testLockUserByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.lockUserByLogin(null));
    }

    @Test
    public void testUnlockUserByLogin() throws Exception {
        USER_SERVICE.lockUserByLogin(USER_TEST_LOGIN);
        USER_SERVICE.unlockUserByLogin(USER_TEST_LOGIN);
        @NotNull final User user = USER_SERVICE.findByLogin(USER_TEST_LOGIN);
        Assert.assertEquals(false, user.getLocked());
    }

    @Test
    public void testUnlockUserByLoginNegative() {
        Assert.assertThrows(LoginEmptyException.class, () -> USER_SERVICE.unlockUserByLogin(null));
    }

    @Test
    public void testUpdateUser() throws Exception {
        @NotNull final String FIO = "FIO";
        USER_SERVICE.updateUser(USER_TEST.getId(), FIO, FIO, FIO);
        @NotNull final User user2 = USER_SERVICE.findByLogin(USER_TEST_LOGIN);
        Assert.assertEquals(FIO, user2.getFirstName());
        Assert.assertEquals(FIO, user2.getMiddleName());
        Assert.assertEquals(FIO, user2.getLastName());
    }

    @Test
    public void testUpdateUserNegative() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> USER_SERVICE.updateUser(null, "STRING_FAKE", "STRING_FAKE", "STRING_FAKE"));
        Assert.assertThrows(UserNotFoundException.class, () -> USER_SERVICE.updateUser("STRING_FAKE", "STRING_FAKE", "STRING_FAKE", "STRING_FAKE"));
    }

}