package ru.t1.schetinin.tm.test.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.schetinin.tm.api.service.*;
import ru.t1.schetinin.tm.enumerated.Sort;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.schetinin.tm.exception.field.DescriptionEmptyException;
import ru.t1.schetinin.tm.exception.field.IdEmptyException;
import ru.t1.schetinin.tm.exception.field.NameEmptyException;
import ru.t1.schetinin.tm.exception.user.UserIdEmptyException;
import ru.t1.schetinin.tm.marker.UnitCategory;
import ru.t1.schetinin.tm.model.Project;
import ru.t1.schetinin.tm.model.User;
import ru.t1.schetinin.tm.service.*;
import ru.t1.schetinin.tm.util.HashUtil;

import java.util.List;

@Category(UnitCategory.class)
public class ProjectServiceTest {

    @NotNull
    public final static Project PROJECT_TEST1 = new Project();

    @NotNull
    public final static Project PROJECT_TEST2 = new Project();

    @NotNull
    public final static Project PROJECT_TEST3 = new Project();

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    @NotNull
    private static String USER_ID = "";

    @NotNull
    private static final IProjectService PROJECT_SERVICE = new ProjectService(CONNECTION_SERVICE);

    @NotNull
    private static final ITaskService TASK_SERVICE = new TaskService(CONNECTION_SERVICE);

    @NotNull
    private static final IUserService USER_SERVICE = new UserService(PROPERTY_SERVICE, CONNECTION_SERVICE, PROJECT_SERVICE, TASK_SERVICE);

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final User user = new User();
        user.setLogin("test_login");
        @Nullable final String hash = HashUtil.salt(PropertyServiceTest.PROPERTY_SERVICE, "test_password");
        Assert.assertNotNull(hash);
        user.setPasswordHash(hash);
        USER_SERVICE.add(user);
        USER_ID = user.getId();
        PROJECT_TEST1.setName("Test_project_1");
        PROJECT_TEST1.setDescription("description_1");
        PROJECT_TEST2.setName("Test_project_2");
        PROJECT_TEST2.setDescription("description_2");
        PROJECT_TEST3.setName("Test_project_3");
        PROJECT_TEST3.setDescription("description_3");
    }

    @AfterClass
    public static void tearDown() throws Exception {
        @Nullable final User user = USER_SERVICE.findByLogin("test_login");
        if (user != null) USER_SERVICE.remove(user);
    }

    @Before
    public void initDemoData() throws Exception {
        PROJECT_SERVICE.add(USER_ID, PROJECT_TEST1);
        PROJECT_SERVICE.add(USER_ID, PROJECT_TEST2);
    }

    @After
    public void clearData() throws Exception {
        PROJECT_SERVICE.clear(USER_ID);
    }

    @Test
    public void testCreate() throws Exception {
        @NotNull final Project project = PROJECT_SERVICE.create(USER_ID, PROJECT_TEST3.getName());
        Assert.assertEquals(USER_ID, project.getUserId());
        Assert.assertEquals(PROJECT_TEST3.getName(), project.getName());
    }

    @Test
    public void testCreateWithDesc() throws Exception {
        @NotNull final Project project = PROJECT_SERVICE.create(USER_ID, PROJECT_TEST3.getName(), PROJECT_TEST3.getDescription());
        Assert.assertEquals(USER_ID, project.getUserId());
        Assert.assertEquals(PROJECT_TEST3.getName(), project.getName());
        Assert.assertEquals(PROJECT_TEST3.getDescription(), project.getDescription());
    }

    @Test
    public void testUpdateById() throws Exception {
        @NotNull final String name = "ProjectUpdate";
        @NotNull final String desc = "descUpdate";
        PROJECT_SERVICE.updateById(USER_ID, PROJECT_TEST1.getId(), name, desc);
        @Nullable final Project project = PROJECT_SERVICE.findOneById(USER_ID, PROJECT_TEST1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(desc, project.getDescription());

    }

    @Test
    public void testChangeProjectStatusById() throws Exception {
        @NotNull final Status status = Status.IN_PROGRESS;
        PROJECT_SERVICE.changeProjectStatusById(USER_ID, PROJECT_TEST1.getId(), status);
        @Nullable final Project project = PROJECT_SERVICE.findOneById(USER_ID, PROJECT_TEST1.getId());
        Assert.assertEquals(status, project.getStatus());
    }

    @Test
    public void testCreateNameNull() {
        @NotNull final String userId = USER_ID;
        @Nullable final String projectName = null;
        @NotNull final String desc = "desc";
        Assert.assertThrows(NameEmptyException.class, () -> PROJECT_SERVICE.create(userId, projectName));
        Assert.assertThrows(NameEmptyException.class, () -> PROJECT_SERVICE.create(userId, projectName, desc));
    }

    @Test
    public void testCreateUserIdNull() {
        @Nullable final String userId = null;
        @NotNull final String projectName = "ProjectNullId";
        @NotNull final String desc = "desc";
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.create(userId, projectName));
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.create(userId, projectName, desc));
    }

    @Test
    public void testCreateDescriptionNull() {
        @NotNull final String userId = USER_ID;
        @NotNull final String projectName = "ProjectNullId";
        @Nullable final String desc = null;
        Assert.assertThrows(DescriptionEmptyException.class, () -> PROJECT_SERVICE.create(userId, projectName, desc));
    }

    @Test
    public void testUpdateByIdNegative() throws Exception {
        @NotNull final String userId = USER_ID;
        @NotNull final String userIdFake = "USER_ID_FAKE";
        @NotNull final Project project = PROJECT_TEST1;
        @NotNull final String name = "ProjectUpdate";
        @NotNull final String desc = "descUpdate";
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.updateById(null, project.getId(), name, desc));
        Assert.assertThrows(IdEmptyException.class, () -> PROJECT_SERVICE.updateById(userId, null, name, desc));
        Assert.assertThrows(NameEmptyException.class, () -> PROJECT_SERVICE.updateById(userId, project.getId(), null, desc));
        Assert.assertThrows(DescriptionEmptyException.class, () -> PROJECT_SERVICE.updateById(userId, project.getId(), name, null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> PROJECT_SERVICE.updateById(userIdFake, project.getId(), name, desc));
    }

    @Test
    public void testChangeProjectStatusByIdNegative() throws Exception {
        @NotNull final String userId = USER_ID;
        @NotNull final String userIdFake = "USER_ID_FAKE";
        @NotNull final Project project = PROJECT_TEST1;
        @NotNull final Status status = Status.IN_PROGRESS;
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.changeProjectStatusById(null, project.getId(), status));
        Assert.assertThrows(IdEmptyException.class, () -> PROJECT_SERVICE.changeProjectStatusById(userId, null, status));
        Assert.assertThrows(ProjectNotFoundException.class, () -> PROJECT_SERVICE.changeProjectStatusById(userIdFake, project.getId(), status));
    }

    @Test
    public void testClear() throws Exception {
        PROJECT_SERVICE.clear(USER_ID);
        Assert.assertEquals(0, PROJECT_SERVICE.getSize(USER_ID));
    }

    @Test
    public void testClearUser() throws Exception {
        PROJECT_SERVICE.clear(USER_ID);
        Assert.assertEquals(0, PROJECT_SERVICE.getSize(USER_ID));
    }

    @Test
    public void testFindAll() throws Exception {
        @NotNull final List<Project> projects = PROJECT_SERVICE.findAll(USER_ID);
        Assert.assertEquals(projects.size(), PROJECT_SERVICE.getSize(USER_ID));
    }

    @Test
    public void testAddProject() throws Exception {
        Assert.assertNotNull(PROJECT_SERVICE.add(USER_ID, PROJECT_TEST3));
        @Nullable final Project project = PROJECT_SERVICE.findOneById(USER_ID, PROJECT_TEST3.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(PROJECT_TEST3.getId(), project.getId());
    }

    @Test
    public void testFindAllSort() throws Exception {
        @NotNull final String sortType = "BY_CREATED";
        @NotNull final Sort sort = Sort.toSort(sortType);
        @NotNull final List<Project> projectSort = PROJECT_SERVICE.findAll(USER_ID, sort.getComparator());
        Assert.assertNotNull(PROJECT_SERVICE.findAll(USER_ID, sort.getComparator()));
        Assert.assertEquals(PROJECT_SERVICE.findAll(USER_ID).toString(), projectSort.toString());
    }

    @Test
    public void testExistById() throws Exception {
        Assert.assertTrue(PROJECT_SERVICE.existsById(USER_ID, PROJECT_TEST1.getId()));
    }

    @Test
    public void testFindOneById() throws Exception {
        @NotNull final List<Project> projects = PROJECT_SERVICE.findAll(USER_ID);
        @NotNull final Project project1 = projects.get(0);
        @NotNull final String projectId = project1.getId();
        Assert.assertEquals(project1.toString(), PROJECT_SERVICE.findOneById(USER_ID, projectId).toString());
    }

    @Test
    public void testRemoveById() throws Exception {
        PROJECT_SERVICE.removeById(USER_ID, PROJECT_TEST2.getId());
        Assert.assertNull(PROJECT_SERVICE.findOneById(USER_ID, PROJECT_TEST2.getId()));
    }

    @Test
    public void testRemoveByIdUserNegative() throws Exception {
        PROJECT_SERVICE.remove(USER_ID, PROJECT_TEST2);
        Assert.assertThrows(IdEmptyException.class, () -> PROJECT_SERVICE.removeById(USER_ID, null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> PROJECT_SERVICE.removeById("USER_ID_FAKE", PROJECT_TEST2.getId()));
    }

}